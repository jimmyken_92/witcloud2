from core.modules.VideoReader import VideoReader
from core.modules.DetectionDrawer import DetectionDrawer
from core.modules.DetectionPacker import DetectionPacker
from core.modules.VideoWriter import VideoWriter

# to test
from core.modules.FaceDetector import FaceDetector
from core.modules.FaceComparator import FaceComparator

import os
import sys
import unittest
from timeit import time

class TestFaceRecognition(unittest.TestCase):
	def test_face_recognition(self):
		# set the (video)source path
		file = 'src/test.mp4'
		output_path = 'output/'

		# set components and classes
		components = []

		videoReader = VideoReader(filename=file) # id
		detectionDrawer = DetectionDrawer(output=output_path)
		detectionPacker = DetectionPacker(output=output_path)
		videoWriter = VideoWriter(output=output_path)
		faceDetector = FaceDetector(LABEL='face_recognition')
		faceComparator = FaceComparator()

		# add components
		components.append(videoReader)

		# set subscriptions -> expected results
		videoReader.subscribe(faceDetector) # -> frame
		faceDetector.subscribe(faceComparator) # -> (boxes, encodings, frame)
		faceComparator.subscribe(detectionDrawer) # -> (lst_detections, frame)
		faceComparator.subscribe(detectionPacker) # -> (lst_detections, frame)
		detectionDrawer.subscribe(videoWriter)

		# start running components
		print('Started')
		for component in components:
			component.start()

		for component in components:
			component.join()
		print('Finished')

		# when finished, close the videoWriter
		videoWriter.close()
		# start writing detections in a .json file
		detectionPacker.dumpDetectionsToFile()

if __name__ == '__main__':
	unittest.main()

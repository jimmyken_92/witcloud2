import sys
import os
import stat
import re
import time
import subprocess as sp
import json
import warnings

import numpy as np

class FFmpegWriter():
    def __init__(self, filename, inputdict=None, outputdict=None, verbosity=0):
        '''
        Parameters
        ----------
        filename: string
            Video file path for writing
        inputdict: dict
            Input dictionary parameters, i.e. how to interpret the data coming from python.
        outputdict: dict
            Output dictionary parameters, i.e. how to encode the data 
            when writing to file.
        Returns
        -------
        none
        '''
        self.DEVNULL = open(os.devnull, 'wb')
        # filename = os.path.abspath(filename)
        _, self.extension = os.path.splitext(filename)
        basepath, _ = os.path.split(filename)

        if not inputdict:
            inputdict = {}

        if not outputdict:
            outputdict = {}

        self._filename = filename
        _, self.extension = os.path.splitext(self._filename)

        # extract extension used (only used for raw outputs)
        self.inputdict = inputdict
        self.outputdict = outputdict

        self.verbosity = verbosity

        if '-f' not in self.inputdict:
            self.inputdict['-f'] = 'rawvideo'
        self.warmStarted = 0

    def _warmStart(self, M, N, C):
        self.warmStarted = 1

        if '-pix_fmt' not in self.inputdict:
            # check the number channels to guess 
            if C == 1:
                self.inputdict['-pix_fmt'] = 'gray'
            elif C == 2:
                self.inputdict['-pix_fmt'] = 'ya8'
            elif C == 3:
                self.inputdict['-pix_fmt'] = 'rgb24'
            elif C == 4:
                self.inputdict['-pix_fmt'] = 'rgba'
            # else:
            #     raise NotImplemented

        # self.bpp = bpplut[self.inputdict['-pix_fmt']][1]
        # self.inputNumChannels = bpplut[self.inputdict['-pix_fmt']][0]
        self.inputNumChannels = 3
        
        assert self.inputNumChannels == C, 'Failed to pass the correct number of channels %d for the pixel format %s.' % (self.inputNumChannels, self.inputdict['-pix_fmt'])  

        if ('-s' in self.inputdict):
            widthheight = self.inputdict['-s'].split('x')
            self.inputwidth = np.int(widthheight[0])
            self.inputheight = np.int(widthheight[1])
        else: 
            self.inputdict['-s'] = str(N) + 'x' + str(M)
            self.inputwidth = N
            self.inputheight = M

        # prepare output parameters, if raw
        if self.extension == '.yuv':
            if '-pix_fmt' not in self.outputdict:
                self.outputdict['-pix_fmt'] = 'yuvj444p'
                if self.verbosity != 0:
                    warnings.warn('No output color space provided. Assuming yuvj420p.', UserWarning)

        # create input args
        iargs = []
        for key in self.inputdict.keys():
            iargs.append(key)
            iargs.append(self.inputdict[key])

        oargs = []
        for key in self.outputdict.keys():
            oargs.append(key)
            oargs.append(self.outputdict[key])

        cmd = ['ffmpeg', '-y'] + iargs + ['-i', '-'] + oargs + [self._filename]

        print(''.join(cmd))
        self._cmd = ' '.join(cmd)

        # launch process
        if self.verbosity == 0:
            self._proc = sp.Popen(cmd, stdin=sp.PIPE, stdout=self.DEVNULL, stderr=sp.STDOUT)
        else:
            print(self._cmd)
            self._proc = sp.Popen(cmd, stdin=sp.PIPE, stdout=sp.PIPE, stderr=None)

    def close(self):
        # closes the video and terminates FFmpeg process
        if not hasattr(self, '_proc'): # pragma: no cover
            raise Exception('ffmpeg is not working')

        if self._proc.poll() is not None:
            return

        if self._proc.stdin:
            self._proc.stdin.close()

        self._proc.wait()
        self._proc = None
        self.DEVNULL.close()

    def writeFrame(self, im):
        # sends ndarray frames to FFmpeg
        vid = self.vshape(im)
        T, M, N, C = vid.shape
        if not self.warmStarted:
            self._warmStart(M, N, C)

        # ensure that ndarray image is in uint8
        vid[vid > 255] = 255
        vid[vid < 0] = 0
        vid = vid.astype(np.uint8)

        # check size of image
        if M != self.inputheight or N != self.inputwidth:
            raise ValueError('All images in a movie should have same size')
        if C != self.inputNumChannels:
            raise ValueError('All images in a movie should have same number of channels')
        assert self._proc is not None # check status

        # write
        try:
            self._proc.stdin.write(vid.tostring())
        except IOError as e:
            # show the command and stderr from pipe
            msg = '{0:}\n\nFFMPEG COMMAND:\n{1:}\n\nFFMPEG STDERR OUTPUT:\n'.format(e, self._cmd)
            raise IOError(msg)

    def vshape(self,videodata):
        '''Standardizes the input data shape.
        Transforms video data into the standardized shape (T, M, N, C), where
        T is number of frames, M is height, N is width, and C is number of 
        channels.
        Parameters
        ----------
        videodata : ndarray
            Input data of shape (T, M, N, C), (T, M, N), (M, N, C), or (M, N), where
            T is number of frames, M is height, N is width, and C is number of 
            channels.
        Returns
        -------
        videodataout : ndarray
            Standardized version of videodata, shape (T, M, N, C)
        '''
        if not isinstance(videodata, np.ndarray):
            videodata = np.array(videodata)

        if len(videodata.shape) == 2:
            a, b = videodata.shape
            return videodata.reshape(1, a, b, 1)
        elif len(videodata.shape) == 3:
            a, b, c = videodata.shape
            # check the last dimension small
            # interpret as color channel
            if c in [1, 2, 3, 4]:
                return videodata.reshape(1, a, b, c)
            else:
                return videodata.reshape(a, b, c, 1)
        elif len(videodata.shape) == 4:
            return videodata
        else:
            raise ValueError('Improper data input')

import time

class ComponentsController(object):
    def __init__(self):
        self.subscribers = []

    def subscribe(self, subscriber, list_subscribers=None):
        if list_subscribers is None:
            self.subscribers.append(subscriber)
        else:
            list_subscribers.append(subscriber)

    def unsubscribe(self, subscriber):
        self.subscribers.remove(subscriber)

    def send(self, data, list_subscribers=None):
        if list_subscribers is None:
            list_subscribers = self.subscribers
        for subscriber in list_subscribers:
            if callable(subscriber):
                subscriber(data)
            else:
                # using the function get() in subscriber(s) to receive the data
                subscriber.get(data)

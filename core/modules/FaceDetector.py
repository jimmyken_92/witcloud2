from core.models.Detection import Detection
from core.models.Frame import Frame
from core.modules.ComponentsController import ComponentsController

import face_recognition

class FaceDetector(ComponentsController):
	def __init__(self, LABEL=None):
		ComponentsController.__init__(self)
		print('### FaceDetector Started ###')

		self.LABEL = LABEL

	def recogniseFace(self, received_frame):
		if received_frame is not None:
			# print('INFO:FaceDetector: Recognise face')
			frame = received_frame.data
			faces = []

			if self.LABEL == 'face_recognition':
				# recognise face
				detected_faces = face_recognition.face_locations(frame)
				# encode data
				encodings = face_recognition.face_encodings(frame, detected_faces)

				# reorder list
				for top, right, bottom, left in detected_faces:
					x1, x2, y1, y2 = left, right, top, bottom
					faces.append([x1, x2, y1, y2])

				self.send((faces, encodings, received_frame))
			else:
				# recognise face
				detected_faces = face_recognition.face_locations(frame)

				# reorder list
				for top, right, bottom, left in detected_faces:
					x1, x2, y1, y2 = left, right, top, bottom
					faces.append([x1, x2, y1, y2])

				# return the detected detected faces
				self.send((faces, received_frame))

	# receiving data
	def get(self, frame:Frame):
		self.recogniseFace(frame)

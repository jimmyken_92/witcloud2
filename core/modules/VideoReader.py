from core.models.Frame import Frame
from core.modules.ComponentsController import ComponentsController

import cv2
import threading
from time import time

class VideoReader(ComponentsController, threading.Thread):
	def __init__(self, filename): # id
		ComponentsController.__init__(self)
		threading.Thread.__init__(self)

		# set the variables
		self.filename = filename
		self.frame = None

	def newFrame(self, frame):
		try:
			# transform BGR format to RGB and grey
			frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
			grey = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)

			# using Frame model to pass the data
			self.frame = Frame(
				data=frame,
				grey=grey,
				timestamp=str(time()),
				video_id=0,
				image_id=None
			)
			return self.frame
		except:
			return None

	def run(self):
		try:
			# assign VideoCapture(0) by defect
			if int(self.filename) == 0:
				file = 0
			# assign VideoCapture(**) -> **1, 2, 3...
			elif int(self.filename) > 0:
				file = int(self.filename)
			# assign VideoCapture(path_to_the_video)
		except:
			file = self.filename

		# start reading video
		capture = cv2.VideoCapture(file)
		while True:
			frame = capture.read()[1]
			newFrame = self.newFrame(frame)
			if newFrame is not None:
				self.send(newFrame)
			else:
				print('Error making frame')

from core.models.Detection import Detection
from core.models.Frame import Frame
from core.modules.ComponentsController import ComponentsController
from core.util import Paths

import cv2
import pickle # or json
import face_recognition
from time import time
from hashlib import sha256

class FaceComparator(ComponentsController):
	def __init__(self):
		ComponentsController.__init__(self)
		print('### FaceComparator Started ###')

		# load faces data from pickle (or json or DB)
		self.data = pickle.loads(open(Paths.facesData(), 'rb').read())

	# generate random ID for new detected faces
	def randomID(self, encoding):
		create_id = sha256(str(encoding).encode()).hexdigest()
		return str(create_id[:5])

	def comparator(self, data):
		if data is not None:
			boxes, encodings, received_frame = data
			# print('INFO:FaceComparator: Compare faces')
			frame = received_frame.data
			video_id = received_frame.video_id

			lst_detections = []
			names = []

			for encoding in encodings:
				if self.data:
					matches = face_recognition.compare_faces(self.data['encodings'], encoding)
					name = 'unknown'

					if True in matches:
						matches_id = [i for (i, b) in enumerate(matches) if b]
						counts = {}

						for i in matches_id:
							name = self.data['names'][i]
							counts[name] = counts.get(name, 0) + 1
						name = max(counts, key=counts.get)

					else:
						self.data['encodings'].append(encoding)
						self.data['names'].append(self.randomID(encoding))

					# if name != 'unknown':
					names.append(name)
				else:
					self.data['encodings'].append(encoding)
					self.data['names'].append(self.randomID(encoding))

			# if len(names) > 0:
			for ((x1, x2, y1, y2), name) in zip(boxes, names):
				detections_data = Detection(
					label=str(name),
					roi_face=(x1, x2, y1, y2),
					timestamp=str(time()),
					frame_id=0,
					video_id=0
				)
				lst_detections.append(detections_data)
			self.send((lst_detections, received_frame))

	# receiving data
	def get(self, data):
		self.comparator(data)

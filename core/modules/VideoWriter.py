from core.models.Frame import Frame
from core.modules.ComponentsController import ComponentsController
from core.util.FFmpegWriter import FFmpegWriter

import sys

class VideoWriter(ComponentsController):
	def __init__(self, output):
		ComponentsController.__init__(self)
		print('### VideoWriter Started ###')

		self.Flag = True
		self.filename = output + 'result.mp4'
		self.frames = []

		self.videoWriter = FFmpegWriter(filename=self.filename)

	# receiving data
	def get(self, frame):
		try:
			fr = frame.data.copy()
			self.videoWriter.writeFrame(fr)
		except Exception as e:
			sys.stderr.write()

	# finish the process			
	def close(self):
		self.videoWriter.close()

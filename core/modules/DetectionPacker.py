from core.models.Detection import Detection
from core.modules.ComponentsController import ComponentsController

import os
import json
from time import time

class DetectionPacker(ComponentsController):
    def __init__(self, output):
        ComponentsController.__init__(self)

        self.filename = output + 'result.json'
        self.list_detections = []

    # dump the list of detections from memory to a .json file
    def dumpDetectionsToFile(self):
        jsonlist = []
        print('### DetectionPacker Started ###')

        if self.list_detections is not None:
            detections = self.list_detections
            try:
                if not os.path.exists(os.path.dirname(self.filename)):
                    os.mkdir(os.path.dirname(self.filename))

                with open(self.filename, 'w+') as outfile:
                    for detection in detections:
                        for obj in detection:
                            jsonlist.append(obj.__dict__)

                    json.dump(jsonlist, outfile, indent=4)

                print('Saved detections in file --> %s' % self.filename)
            except Exception as e:
                print('ERROR:Error while saving detection file by detection packer --> %s' % str(e))
        else:
            print('Nothing found in detections list')
    
    # receiving data
    def get(self, data):
        detection, frame = data
        # save the received data in memory
        self.list_detections.append(detection)
        print('INFO:DetectionPacker: Received a new detection')

# Witcloud
Witcloud is a AI core separated into pluggable modules based on *publish/subscribe* scalable architecture. This makes it possible to *plug* any other module, in this example I include `face_recognition` module to test.

## Requirements
See `requirements.txt` `pip install requirements.txt`
- [opencv-python](https://opencv.org/) `pip install opencv-python`
- [face_recognition](https://palletsprojects.com/p/flask/) `pip install face_recognition`

## How to use
1. Run `python testFaceRecognition.py` to start

## Working on deployment based on
- GCloud
- Docker

